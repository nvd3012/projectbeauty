package com.hunger.projectbeauty.paging

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.remote.api.FlickrService
import com.hunger.projectbeauty.data.remote.api.FlickrService.Companion.LINK_API_KEY
import com.hunger.projectbeauty.data.model.ListPhotoModel
import com.hunger.projectbeauty.utils.JsoupUtils
import kotlinx.coroutines.*
import java.lang.Exception

class FlickrBoundaryCallback(
    private val service: FlickrService,
    private val scope: CoroutineScope,
    private val handleResponse: (ListPhotoModel?, Int) -> Unit,
    private val netWorkPageSize: Int
) : PagedList.BoundaryCallback<PhotoEntity>() {
    private val TAG = "MainViewModel"
    private var isProgressing = false

    val networkState = MutableLiveData<NetworkState>()

    override fun onZeroItemsLoaded() {
        Log.e(TAG, "onZeroItemsLoaded")
        requestFlickrService(netWorkPageSize, 1, scope)
    }

    override fun onItemAtEndLoaded(itemAtEnd: PhotoEntity) {
        Log.e(TAG, "onItemAtEndLoaded ${itemAtEnd.page}")
        requestFlickrService(netWorkPageSize, itemAtEnd.page + 1, scope)
    }


    private fun requestFlickrService(netWorkPageSize: Int, page: Int, scope: CoroutineScope) {
        scope.launch {
            try {
                if (!isProgressing) {
                    isProgressing = true
                    networkState.postValue(NetworkState.LOADING)
                    val data =
                        service.loadData(
                            page = page, perPage = netWorkPageSize, apiKey =
//                                "3491d57ae6a74c5a69e93a89060f17fe"
                            JsoupUtils.getApiKey(
                                service.apiKEY(
                                    LINK_API_KEY
                                )
                            )
                        )
                    Log.e("Error data", data.toString())
                    if (data.isSuccessful) {
                        data.body().let { response ->
                            if (response!!.stat.equals("ok")) {
                                handleResponse(response.listPhoto, page)
                                networkState.postValue(NetworkState.LOADED)
                            } else {
                                networkState.postValue(NetworkState.error("Invalid API Key (Key has invalid format)"))
                            }
                        }
                    } else {
                        networkState.postValue(NetworkState.error(data.message()))
                    }
//                        delay(1000)
                    isProgressing = false
                    Log.e("Error data", "progressing $isProgressing")
                } else {
                    return@launch
                }
            } catch (e: Exception) {
                isProgressing = false
                networkState.postValue(NetworkState.error("request error ${e.message}"))
            }

        }
    }


}