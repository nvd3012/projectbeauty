package com.hunger.projectbeauty.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

fun TextView.addDrawable(idDrawable: Int, color: Int, padding: Int, size:Int) {
    val imgDrawable = ContextCompat.getDrawable(context,idDrawable)
    imgDrawable?.setBounds(0, 0, size, size)
    imgDrawable?.colorFilter = PorterDuffColorFilter(ContextCompat.getColor(context,color), PorterDuff.Mode.SRC_IN)
    compoundDrawablePadding = padding
    setCompoundDrawablesWithIntrinsicBounds(imgDrawable, null, null, null)
}

fun showEmptyList(show: Boolean, tvNoResult: TextView, rv: RecyclerView) {
    if (show) {
        tvNoResult.visibility = View.VISIBLE
        rv.visibility = View.GONE
    } else {
        tvNoResult.visibility = View.GONE
        rv.visibility = View.VISIBLE
    }
}