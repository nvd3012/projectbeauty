package com.hunger.projectbeauty.utils

import androidx.recyclerview.widget.DiffUtil

object Constants {
    const val ALBUM_ARGUMENT = "ALBUM_ARGUMENT"
    const val ID_PHOTO = "ID_PHOTO"
    const val ID_ALBUM = "ID_ALBUM"
    const val NAME_ALBUM = "NAME_ALBUM"
    const val API_KEY = "API"
    const val TIME = "TIME"
    const val PAGE_CURRENT = "CURRENT"
    const val REQ_CODE_SPEECH_INPUT = 10
    const val TAG_DIALOG_FRAGMENT = "DIALOG_FRAGMENT"
    const val KEY_IMAGE_URL = "KEY_IMG_URL"
    const val LIKED = "LIKED"
    const val PHOTO_ARGUMENT = "PHOTO_ENTITY"

    @JvmField val VERBOSE_NOTIFICATION_CHANNEL_NAME: CharSequence =
        "Verbose WorkManager Notifications"
    const val VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION =
        "Shows notifications whenever work starts"
    @JvmField val NOTIFICATION_TITLE: CharSequence = "Download"
    const val CHANNEL_ID = "VERBOSE_NOTIFICATION"
    const val NOTIFICATION_ID = 1
}

