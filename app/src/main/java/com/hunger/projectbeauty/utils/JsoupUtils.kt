package com.hunger.projectbeauty.utils

import okhttp3.ResponseBody
import org.jsoup.Jsoup

object JsoupUtils {

    fun getApiKey(data: ResponseBody): String {
        val document = Jsoup.parse(data.string())
        document.getElementsByTag("script").forEach {
            val htmlSite = it.data()
            if (htmlSite.contains("root.YUI_config.flickr.api.site_key")) {
                val dataSearch = "api.site_key"
                val indexStart = htmlSite.indexOfAny(listOf(dataSearch)) + dataSearch.length + 4
                return htmlSite.substring(indexStart, indexStart + 32)
            }
        }
        return "none"
    }
}