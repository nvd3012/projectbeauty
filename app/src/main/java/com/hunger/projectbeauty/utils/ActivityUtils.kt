package com.hunger.projectbeauty.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.util.DisplayMetrics
import androidx.swiperefreshlayout.widget.CircularProgressDrawable

object ActivityUtils {


    fun isOrentionScreenPortrait(context: Context): Boolean = context.resources.configuration.orientation==Configuration.ORIENTATION_PORTRAIT

    fun getWidthHeightScreen(context: Context): DisplayMetrics {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics
    }




}