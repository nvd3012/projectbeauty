package com.hunger.projectbeauty.utils

interface Listener<T> {
    fun passData(data: T)
}