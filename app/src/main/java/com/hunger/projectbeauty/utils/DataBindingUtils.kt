package com.hunger.projectbeauty.utils

import android.util.Log
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.flexbox.FlexboxLayoutManager
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import kotlinx.coroutines.*
import java.io.File
import java.lang.Exception


@BindingAdapter(
    value = ["item", "contain"],
    requireAll = false
)
fun setImageWithGlide(
    view: ImageView,
    item: PhotoEntity?,
    contain: RelativeLayout
) {

    GlobalScope.launch(Dispatchers.Main) {
        try {
//            delay(300)
            val lp = contain.layoutParams
            lp.width = item?.width ?: 0
            lp.height = item?.height ?: 0
            if (lp is FlexboxLayoutManager.LayoutParams) {
                lp.flexGrow = 1f
            }
            Glide.with(view.context)
                .load(item?.url_w)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade(300))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(view.context.resources.getDrawable(R.drawable.ic_error, null))
                .into(view)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}

@BindingAdapter("imgUrl")
fun loadImageUrl(imageView: ImageView, url: String?) {
    val context = imageView.context
    val circularProgressDrawable = AnimationUtils.circularProgressDrawable(context, R.color.grey_500)
    circularProgressDrawable.start()
    Glide.with(context).load(url)
        .error(ContextCompat.getDrawable(context, R.drawable.ic_error))
        .placeholder(circularProgressDrawable)
        .transition(DrawableTransitionOptions.withCrossFade(DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()
        .into(imageView)
}

@BindingAdapter("imgUrl")
fun loadImageUrl(imageView: PhotoView, url: String?) {
    Log.e("loadUrl","$url")
    val context = imageView.context
    val circularProgressDrawable = AnimationUtils.circularProgressDrawable(context, R.color.grey_500)
    circularProgressDrawable.start()
    Glide.with(context).load(url)
        .error(ContextCompat.getDrawable(context, R.drawable.ic_error))
        .placeholder(circularProgressDrawable)
        .transition(DrawableTransitionOptions.withCrossFade())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()
        .into(imageView)
}




@BindingAdapter("setWidthHeight", requireAll = true)
fun setWidthHeightLayoutFlexBox(relativeLayout: RelativeLayout, item: PhotoEntity?) {

    val lp = relativeLayout.layoutParams
    lp.width = item?.width ?: 0
    lp.height = item?.height ?: 0
    if (lp is FlexboxLayoutManager.LayoutParams) {
        lp.flexGrow = 1f
    }


}