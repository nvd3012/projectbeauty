package com.hunger.projectbeauty.utils

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.hunger.projectbeauty.R
import java.lang.Exception

object AnimationUtils {
    fun animationFade(view: View) {
        try {
            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = DecelerateInterpolator() //add this
            fadeIn.duration = 500
            val animation = AnimationSet(false)
            animation.addAnimation(fadeIn)
            view.animation = animation
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun circularProgressDrawable(context: Context, color: Int): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 10f
        circularProgressDrawable.centerRadius = 40f
        circularProgressDrawable.setColorSchemeColors(
            ContextCompat.getColor(
                context,
                color
            )
        )
        return circularProgressDrawable
    }

    fun animationMoveY(view: View, hide: Boolean, moveUp: Boolean) {
        val moveY = if (moveUp) {
            if (hide) (2 * view.height) else 0
        } else {
            if (hide) -(2 * view.height) else 0
        }
        view.animate().translationY(moveY.toFloat()).setStartDelay(100).setDuration(300).start()
    }

    //    fun animateNavigation(hide: Boolean) {
//        if (isNavigationHide && hide || !isNavigationHide && !hide) return
//        isNavigationHide = hide
//        val moveY = if (hide) (2 * bottomNavigation.height) else 0
//        bottomNavigation.animate().translationY(moveY.toFloat()).setStartDelay(100)
//            .setDuration(300).start()
//
//    }
//
//    fun animateSearchBar(hide: Boolean) {
//        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return
//        isSearchBarHide = hide
//        val moveY = 0
//        if (hide) {
//            searchBar.visibility = View.GONE
//        } else {
//            searchBar.visibility = View.VISIBLE
//        }
//        searchBar.animate().translationY(moveY.toFloat()).setStartDelay(100).setDuration(300)
//            .start()
//    }
}