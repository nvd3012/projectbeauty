package com.hunger.projectbeauty.utils

import android.content.Context
import androidx.core.content.edit
import javax.inject.Inject
import javax.inject.Singleton

class AppSharedPreferences (context: Context){

    private val sharedPreferences = context.getSharedPreferences(
        context.packageName,
        Context.MODE_PRIVATE
    )
    fun setShared(key:String, value:String){
        sharedPreferences.edit { putString(key,value) }
    }
    fun setShared(key:String, value:Int){
        sharedPreferences.edit { putInt(key,value) }
    }

    fun getSharedString(key:String):String?{
        return sharedPreferences.getString(key,null)
    }

    fun getSharedInt(key:String):Int{
        return sharedPreferences.getInt(key,0)
    }

}