package com.hunger.projectbeauty.data.local.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(
    primaryKeys = ["idAlbum", "idPhoto"],
    indices =
    [Index(value = ["idPhoto","idAlbum"])],
    foreignKeys = [ForeignKey(
        entity = PhotoEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("idPhoto")
    ),
        ForeignKey(
            entity = AlbumEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("idAlbum"),
            onDelete = CASCADE
        )
    ]

)
data class MergeTableEntity(@ColumnInfo(name = "idAlbum") val idAlbum: Long, @ColumnInfo(name = "idPhoto") val idPhoto: String)