package com.hunger.projectbeauty.data.model

import androidx.room.ColumnInfo
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.hunger.projectbeauty.data.local.dao.DAO
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

data class AlbumModel constructor(
    @ColumnInfo
    var id: Long = 0,
    @ColumnInfo
    var name: String = "",
    @ColumnInfo
    var dateCreate: Long = 0L,
    @ColumnInfo
    var imageUrl: String? = null,
    @ColumnInfo
    var imageLocal: String? = null,
    @ColumnInfo
    var quantity: Int = 0,
    @Ignore
    var status: DAO? = null
):Serializable {
    constructor(name: String, status: DAO) : this(0, name, 0L, null, null, 0, status)

    fun toSimpleString(): String {
        val format = SimpleDateFormat("dd/MM/yyy", Locale.ENGLISH)
        return format.format(Date(dateCreate))
    }
}