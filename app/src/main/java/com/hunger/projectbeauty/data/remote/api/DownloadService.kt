package com.hunger.projectbeauty.data.remote.api

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.IntentSender
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.utils.Constants.CHANNEL_ID
import com.hunger.projectbeauty.utils.Constants.KEY_IMAGE_URL
import com.hunger.projectbeauty.utils.Constants.NOTIFICATION_ID
import com.hunger.projectbeauty.utils.Constants.NOTIFICATION_TITLE
import com.hunger.projectbeauty.utils.Constants.VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION
import com.hunger.projectbeauty.utils.Constants.VERBOSE_NOTIFICATION_CHANNEL_NAME
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import java.io.*
import javax.inject.Inject

class DownloadService(
    context: Context,
    workerParams: WorkerParameters
) :
    CoroutineWorker(context, workerParams) {
    @Inject
    lateinit var service: FlickrService

    private var notificationManager: NotificationManager? = null
    lateinit var notificationBuilder: NotificationCompat.Builder

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            val name = VERBOSE_NOTIFICATION_CHANNEL_NAME
            val description = VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description

            // Add the channel
            notificationManager =
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

            notificationManager?.createNotificationChannel(channel)
        }

        notificationBuilder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(NOTIFICATION_TITLE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(LongArray(0))
    }

    override suspend fun doWork(): Result = coroutineScope {
        withContext(Dispatchers.IO) {
            try {
                val resourceUrl = inputData.getString(KEY_IMAGE_URL)
                resourceUrl?.let {
                    saveToStorage(service.downloadImageUrl(it), resourceUrl)
                }
                Result.success()
            } catch (e: Exception) {
                e.printStackTrace()
                Result.failure()
            }
        }

    }

    private suspend fun saveToStorage(body: ResponseBody?, fileName: String) {
        coroutineScope {
            withContext(Dispatchers.IO) {
                makeStatusNotification("Download is processing", false)
                try {
                    val dir = File(
                        applicationContext.getExternalFilesDir(
                            null
                        ), "MyPhotos"
                    )
                    if (!dir.exists()) {
                        dir.mkdirs()
                    }
                    val fileImgDownload =
                        File(dir, fileName.substring(fileName.lastIndexOf('/') + 1))
                    var inputStream: InputStream? = null
                    var outputStream: OutputStream? = null
                    try {
                        val fileReader = ByteArray(4096)
                        val fileSize = body?.contentLength()
                        var fileSizeDownloaded: Long = 0

                        inputStream = body?.byteStream()
                        outputStream = FileOutputStream(fileImgDownload)
                        while (true) {
                            val read = inputStream!!.read(fileReader)
                            if (read == -1) {
                                break
                            }
                            outputStream.write(fileReader, 0, read)
                            fileSizeDownloaded += read.toLong()
                            Log.d(
                                "writeResponseBodyToDisk",
                                "file download: $fileSizeDownloaded of $fileSize"
                            )
                        }
                        outputStream.flush()
                        makeStatusNotification("Download finished", true)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        makeStatusNotification("Download error", false)
                    } finally {
                        inputStream?.close()
                        outputStream?.close()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    makeStatusNotification("Download error", false)
                }
            }

        }

    }

    fun makeStatusNotification(message: String, finished: Boolean) {

        // Create the notification
        notificationBuilder.setContentText(message)

        // Show the notification
        notificationManager?.notify(NOTIFICATION_ID, notificationBuilder.build())
    }


}