package com.hunger.projectbeauty.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.paging.PagedList
import androidx.paging.toLiveData
import androidx.room.withTransaction
import com.hunger.projectbeauty.data.executeCudDatabase
import com.hunger.projectbeauty.data.local.AppDatabase
import com.hunger.projectbeauty.data.local.entity.AlbumEntity
import com.hunger.projectbeauty.data.local.entity.MergeTableEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.model.AlbumModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AlbumRepository @Inject constructor(private val db: AppDatabase) {
    private val TAG = "Repository"
    fun insertAlbum(
        scope: CoroutineScope,
        album: AlbumModel,
        onFinished: (Long) -> Unit
    )= executeCudDatabase(scope = scope,databaseQuery = {db.albumDao().insert(AlbumEntity(0, album.name, Date().time))},db = db,onFinished = onFinished )
//    {
//        scope.launch {
//            db.apply {
//                withTransaction {
//                    val row = AlbumEntity(0, album.name, Date().time)
//
//                    try {
//                        val idResult = db.albumDao().insert(row)
//                        if (idResult != -1L) {
//                            album.id = idResult
//                            onFinished(album)
//                        } else {
//                            onError("create album error")
//                        }
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                        onError("error ${e.message}")
//                    }
//
//                }
//            }
//        }
//    }

    fun updateAlbum(
        scope: CoroutineScope,
        album: AlbumEntity,
        onFinished: () -> Unit,
        onError: () -> Unit
    ) {
        scope.launch {
            db.apply {
                withTransaction {
                    if (db.albumDao().update(album) != -1) {
                        onFinished()
                    } else {
                        onError()
                    }
                }
            }
        }
    }

    fun updateAlbumName(
        scope: CoroutineScope,
        model: AlbumModel,
        onFinished: (Int) -> Unit
    )  = executeCudDatabase(scope = scope,databaseQuery = {db.albumDao().updateNameAlbum(model.name,model.id)},onFinished = onFinished,db = db)
//    {
//        scope.launch {
//            db.apply {
//                withTransaction {
//                    try {
//                        val result = db.albumDao().updateNameAlbum(model.name, model.id)
//                        Log.e(TAG, "updateSuccess $result")
//                        if (result != -1) {
//                            onFinished(model)
//                        } else {
//                            onError("update album error")
//                        }
//                    } catch (e: Exception) {
//                        onError("something error ${e.message}")
//                    }
//
//                }
//            }
//        }
//    }


    fun deleteAlbum(
        scope: CoroutineScope,
        album: AlbumEntity,
        onFinished: () -> Unit,
        onError: () -> Unit
    ) {
        scope.launch {
            db.apply {
                withTransaction {
                    if (db.albumDao().delete(album) != -1) {
                        onFinished()
                    } else {
                        onError()
                    }
                }
            }
        }
    }

    fun deletePhotoInAlbum(scope: CoroutineScope,
                           merges: List<MergeTableEntity>,
                           onFinished: () -> Unit) {
        scope.launch {
            db.apply {
                withTransaction {
                    try {
                        val result = db.mergeDao().delete(merges)
                        if (result != -1) {
                            Log.e("INSERT MergeTable", "$result")
                            withContext(Dispatchers.Main){
                                onFinished()
                            }
                        }
                    }catch (e:Exception){
                        e.printStackTrace()
                    }
                }
            }
        }

    }


    fun deleteAlbumByID(
        scope: CoroutineScope,
        album: AlbumModel,
        onFinished: (Int) -> Unit
    ) = executeCudDatabase(scope = scope,databaseQuery = {db.albumDao().deleteAlbumById(album.id)},db = db,onFinished = onFinished)
//    {
//        scope.launch {
//            db.apply {
//                withTransaction {
//                    try {
//                        if (db.albumDao().deleteAlbumById(album.id) != -1) {
//                            onFinished(album)
//                        } else {
//                            onError("Delete error")
//                        }
//                    } catch (e: Exception) {
//                        onError(e.message)
//                    }
//
//                }
//            }
//        }
//    }

    fun insertMergeTable(
        scope: CoroutineScope,
        merges: List<MergeTableEntity>,
        onFinished: (List<Long>) -> Unit
    )=executeCudDatabase(scope = scope,databaseQuery = {db.mergeDao().insert(merges)},db = db,onFinished = onFinished)
//    {
//        scope.launch {
//            db.apply {
//                withTransaction {
//                    try {
//                        val result = db.mergeDao().insert(merges).size
//                        if (result != 0) {
//                            Log.e("INSERT MergeTable", "$result")
//                            onFinished()
//                        } else {
//                            onError()
//                        }
//                    }catch (e:Exception){
//                        e.printStackTrace()
//                        onError()
//                    }
//
//                }
//            }
//        }
//    }

    fun getPhotosOfAlbum(id: Long): LiveData<PagedList<PhotoEntity>> {
        return db.albumDao().getPhotosOfAlbum(id).toLiveData(DATABASE_PAGE_SIZE)
            .distinctUntilChanged()
    }

    fun getListAlbum(): LiveData<PagedList<AlbumModel>> {
        return db.albumDao().getAlbums()
            .toLiveData(DATABASE_PAGE_SIZE).distinctUntilChanged()
    }


    companion object {
        const val DATABASE_PAGE_SIZE = 10
    }
}