package com.hunger.projectbeauty.data.remote.api

import retrofit2.HttpException
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ResponseHandler {
    fun handleErrorRes(exception: Exception): String {
        exception.printStackTrace()
        return when (exception) {
            is HttpException -> "HttpError occurred ${exception.code()}"
            is SocketTimeoutException -> "Connection timed out "
            is UnknownHostException -> "The IP address of a hostname could not be determined"
            else -> exception.message ?: "something error"
        }
    }
}