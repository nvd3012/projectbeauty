package com.hunger.projectbeauty.data.model

import com.google.gson.annotations.SerializedName

data class ResponseDataModel(
    @SerializedName("photos") val listPhoto: ListPhotoModel,
    @SerializedName("stat") val stat: String?
)