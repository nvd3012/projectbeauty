package com.hunger.projectbeauty.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "photos")
data class PhotoEntity(
    @PrimaryKey @SerializedName("id") var id: String,
    @SerializedName("dateupload") @Ignore var dateupload: String,
    @SerializedName("owner") var owner: String,
    @SerializedName("title") var title: String,
    @SerializedName("ownername") var ownername: String,
    @SerializedName("height_w") var height_w: Int,
    @SerializedName("width_w") var width_w: Int,
    @SerializedName("height_z") var height_z: Int,
    @SerializedName("width_z") var width_z: Int,
    @SerializedName("height_m") var height_m: Int,
    @SerializedName("width_m") var width_m: Int,
    @SerializedName("height_l") var height_l: Int,
    @SerializedName("width_l") var width_l: Int,
    @SerializedName("height_c") var height_c: Int,
    @SerializedName("width_c") var width_c: Int,
    @SerializedName("url_w") var url_w: String? = null,
    @SerializedName("url_m") var url_m: String? = null,
    @SerializedName("url_z") var url_z: String? = null,
    @SerializedName("url_c") var url_c: String? = null,
    @SerializedName("url_l") var url_l: String? = null,
    @SerializedName("url_h") var url_h: String? = null,
    @SerializedName("url_k") var url_k: String? = null,
    @Expose(serialize = false) var urlLocal: String? = null,
    @Expose(serialize = false) var dateLong: Long = dateupload.toLong(),
    @Expose(serialize = false) var liked: Int,
    @Expose(serialize = false) @Ignore var height: Int,
    @Expose(serialize = false) @Ignore var width: Int,
    @Expose(serialize = false) var page: Int,
    @Expose(serialize = false) @Ignore var selected: Boolean

) : Serializable {
    constructor() : this(
        "",
        "",
        "",
        "",
        "",
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        0,
        0,
        0,
        0, 0,
        false
    )

    fun setContentItemShow(widthSize: Int) {
        width = widthSize
        height = (widthSize * 1.3).toInt()
//        when (widthSize) {
//            in 700..900 -> {
//                width = width_w
//                height = height_w
//            }
//            in 1000..1200 -> {
//                width = width_z
//                height = height_z
//            }
//            in 1400..1600 -> {
//                width = width_c
//                height = height_c
//            }
//            else -> {
//                width = width_c
//                height = height_c
//            }
//        }
    }
}

