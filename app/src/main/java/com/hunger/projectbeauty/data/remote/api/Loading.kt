package com.hunger.projectbeauty.data.remote.api

import android.app.ProgressDialog
import android.content.Context

import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Carot.
 */
class Loading(context: Context) {

    private val loadingDialog = ProgressDialog(context)
    private val count = AtomicInteger()

    init {
        loadingDialog.setMessage("Please wait...")
        loadingDialog.setCancelable(false)
        loadingDialog.setCanceledOnTouchOutside(false)
    }

    fun show(show: Boolean) {
        if (show) {
            if (count.getAndIncrement() == 0) {
                loadingDialog.show()
            }
        } else {
            if (count.decrementAndGet() == 0 && loadingDialog.isShowing) {
                loadingDialog.dismiss()
            }
            if (count.get() < 0) count.set(0)
        }
    }
}