package com.hunger.projectbeauty.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class AlbumEntity(
    @ColumnInfo
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    @ColumnInfo
    val name: String,
    @ColumnInfo
    val dateCreate: Long
) {
    constructor() : this(0, "", 0L)

}