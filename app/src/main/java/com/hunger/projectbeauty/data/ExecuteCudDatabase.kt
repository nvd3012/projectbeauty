package com.hunger.projectbeauty.data

import android.util.Log
import androidx.room.withTransaction
import com.hunger.projectbeauty.data.local.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception

fun <T>executeCudDatabase(
    scope: CoroutineScope,
    databaseQuery: suspend () -> T,
    db: AppDatabase,
    onFinished: (T) -> Unit
) {
    scope.launch {
        db.withTransaction {
            try {
                val result = databaseQuery()
                Log.e("result","$result")
                onFinished(result)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}


