package com.hunger.projectbeauty.data.repository

import android.content.IntentSender
import android.util.Log
import androidx.lifecycle.*
import androidx.paging.toLiveData
import androidx.room.withTransaction
import com.hunger.projectbeauty.data.executeCudDatabase
import com.hunger.projectbeauty.data.local.AppDatabase
import com.hunger.projectbeauty.data.remote.api.FlickrService


import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.remote.api.FlickrService.Companion.LINK_API_KEY
import com.hunger.projectbeauty.paging.*
import com.hunger.projectbeauty.utils.JsoupUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoRepository @Inject constructor(
    private val service: FlickrService,
    private val db: AppDatabase
) {
    private val TAG = "MainViewModel"

    fun updatePhoto(photoEntity: PhotoEntity, coroutineScope: CoroutineScope) {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                db.withTransaction {
                    db.photoDao().update(photoEntity)
                }
            }
        }
    }


    fun updatePhotoUrLocalById(
        photo: PhotoEntity,
        coroutineScope: CoroutineScope,
        onFinished: (Int) -> Unit
    ) = executeCudDatabase(
        scope = coroutineScope,
        databaseQuery = { db.photoDao().updateUrlLocalById(photo.id, photo.urlLocal!!) },
        db = db,
        onFinished = onFinished
    )

    fun updateLikeById(idPhoto: String, coroutineScope: CoroutineScope, onFinished: (Int) -> Unit) =
        executeCudDatabase(
            scope = coroutineScope,
            databaseQuery = { db.photoDao().updateLikeById(idPhoto) },
            db = db,
            onFinished = onFinished
        )


    fun getPagedListPhotoLocal(
        query: String,
        liked: Boolean,
        coroutineScope: CoroutineScope
    ): Listing<PhotoEntity> {
        Log.e(TAG, "data search %${query.replace(' ', '%')}% liked $liked")
        val dataQuery = "%${query.replace(' ', '%')}%"
        return if (!liked) {
            val boundaryCallback = FlickrBoundaryCallback(
                service = service,
                scope = coroutineScope,
                handleResponse = { data, page ->
                    //insert database
                    data!!.photo.let { photosModel ->
                        coroutineScope.launch {
                            db.withTransaction {
                                photosModel.forEach {
                                    it.page = page
                                }
                                db.photoDao().insert(photosModel)
                            }

                        }
                    }
                },
                netWorkPageSize = NETWORK_PAGE_SIZE
            )
//            val boundaryCallback = TestBoundaryCallback()

            val refreshTrigger = MutableLiveData<Unit>()
            val refreshState = refreshTrigger.switchMap {
                refresh(coroutineScope)
            }
            val data = if (query == "") {
                db.photoDao().getPhotos(dataQuery)
                    .toLiveData(DATABASE_PAGE_SIZE, boundaryCallback = boundaryCallback)
            } else {
                db.photoDao().getPhotos(dataQuery)
                    .toLiveData(DATABASE_PAGE_SIZE).distinctUntilChanged()
            }
            Listing(
                pagedList = data,
                networkState = boundaryCallback.networkState,
                refresh = { refreshTrigger.value = null },
                refreshState = refreshState,
                retry = {}
            )
        } else {
            val networkState = MutableLiveData<NetworkState>()
            networkState.value = NetworkState.LOADED
            Listing(
                pagedList = db.photoDao().getPhotosLiked(dataQuery)
                    .toLiveData(DATABASE_PAGE_SIZE).distinctUntilChanged(),
                networkState = networkState,
                refresh = {},
                refreshState = networkState,
                retry = {}
            )
        }
    }

    fun getPagedListPhotoToAddAlbum(
        idAlbum: Long,
        liked: Int,
        coroutineScope: CoroutineScope
    ): Listing<PhotoEntity> {
        val boundaryCallback = FlickrBoundaryCallback(
            service = service,
            scope = coroutineScope,
            handleResponse = { data, page ->
                //insert database
                data!!.photo.let { photosModel ->
                    coroutineScope.launch {
                        db.withTransaction {
                            photosModel.forEach {
                                it.page = page
                            }
                            db.photoDao().insert(photosModel)
                        }

                    }
                }
            },
            netWorkPageSize = NETWORK_PAGE_SIZE
        )
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = refreshTrigger.switchMap {
            refresh(coroutineScope)
        }

        val data = if (liked==1) {
            db.photoDao().getPhotoToAddAlbum(idAlbum, 1).toLiveData(DATABASE_PAGE_SIZE)
                .distinctUntilChanged()
        } else {
            db.photoDao().getPhotoToAddAlbum(idAlbum, 0)
                .toLiveData(DATABASE_PAGE_SIZE, boundaryCallback = boundaryCallback)
                .distinctUntilChanged()
        }
        return Listing(
            pagedList = data,
            networkState = boundaryCallback.networkState,
            refresh = { refreshTrigger.value = null },
            refreshState = refreshState,
            retry = {}
        )
    }

    private fun refresh(coroutineScope: CoroutineScope): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.LOADING
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val data = service.loadData(
                        1,
                        DATABASE_PAGE_SIZE,
//                    "9cab04dbc714234d8179174233d21364"
                        JsoupUtils.getApiKey(service.apiKEY(LINK_API_KEY))
                    )
                    if (data.isSuccessful) {
                        data.body().let { response ->
                            if (response!!.stat.equals("ok")) {
                                db.withTransaction {
                                    db.photoDao().deleteAllData()
                                    val listPhoto = response.listPhoto.photo.map { photoEntity ->
                                        photoEntity.page = 1
                                        photoEntity
                                    }
                                    db.photoDao().insert(listPhoto)
                                }
                            } else {
                                networkState.postValue(NetworkState.error("Invalid API Key (Key has invalid format)"))
                            }
                        }
                    } else {
                        networkState.postValue(NetworkState.error(data.message()))
                    }

                } catch (e: Exception) {
                    networkState.value = NetworkState.error(e.message)
                }
            }

        }
        return networkState
    }


    companion object {
        private const val DATABASE_PAGE_SIZE = 16
        private const val NETWORK_PAGE_SIZE = 26
    }

}