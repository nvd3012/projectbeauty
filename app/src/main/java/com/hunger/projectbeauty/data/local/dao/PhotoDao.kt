package com.hunger.projectbeauty.data.local.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.hunger.projectbeauty.data.local.entity.AlbumEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity


@Dao
abstract class PhotoDao : BaseDao<PhotoEntity> {
    @Query("DELETE FROM photos")
    abstract suspend fun deleteAllData()

    @Query("SELECT * FROM PHOTOS where (liked = 1) AND (title LIKE :queryString)")
    abstract fun getPhotosLiked(queryString: String): DataSource.Factory<Int, PhotoEntity>

    @Query("SELECT * FROM PHOTOS WHERE title LIKE :queryString ")
    abstract fun getPhotos(queryString: String): DataSource.Factory<Int, PhotoEntity>


    @Query("SELECT * FROM PHOTOS WHERE id like :id LIMIT 1")
    abstract fun getPhotoById(id: String): LiveData<PhotoEntity>

    @Query("SELECT * FROM PHOTOS  WHERE liked in (1,:like)  and id not in (SELECT idPhoto FROM MergeTableEntity WHERE idAlbum=:idAlbum)")
    abstract fun getPhotoToAddAlbum(idAlbum: Long, like: Int): DataSource.Factory<Int, PhotoEntity>

    @Query("UPDATE PHOTOS SET urlLocal = :urlLocal WHERE id = :idPhoto")
    abstract suspend fun updateUrlLocalById(idPhoto: String, urlLocal: String) : Int

    @Query("UPDATE photos SET liked = CASE WHEN liked = 0 THEN  1 ELSE  0 END WHERE id like :idPhoto")
    abstract suspend fun updateLikeById(idPhoto: String):Int
}