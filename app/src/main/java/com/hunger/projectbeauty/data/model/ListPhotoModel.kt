package com.hunger.projectbeauty.data.model

import com.google.gson.annotations.SerializedName
import com.hunger.projectbeauty.data.local.entity.PhotoEntity


data class ListPhotoModel(
    @SerializedName("page") val page: Int,
    @SerializedName("pages") val pages: Int,
    @SerializedName("perpage") val perpage: Int,
    @SerializedName("total") val total: String?,
    @SerializedName("photo") val photo: List<PhotoEntity> = emptyList()
)