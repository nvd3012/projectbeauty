package com.hunger.projectbeauty.data.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hunger.projectbeauty.data.local.entity.AlbumEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.model.AlbumModel
import kotlin.contracts.Returns

@Dao
abstract class AlbumDao : BaseDao<AlbumEntity> {

    @Query("SELECT * from photos where id in (select idPhoto from mergetableentity where idAlbum = :id)")
    abstract fun getPhotosOfAlbum(id: Long): DataSource.Factory<Int, PhotoEntity>

    @Query("SELECT a.*,count(b.idAlbum) quantity,c.url_m imageUrl,c.urlLocal imageLocal FROM AlbumEntity a LEFT JOIN mergetableentity b ON a.id = b.idAlbum LEFT JOIN photos c ON b.idPhoto = c.id group by a.id")
    abstract fun getAlbums(): DataSource.Factory<Int, AlbumModel>

    @Query("UPDATE AlbumEntity SET name = :name WHERE id = :id")
    abstract suspend fun updateNameAlbum(name: String, id: Long): Int

    @Query("DELETE FROM AlbumEntity Where id=:id")
    abstract suspend fun deleteAlbumById(id: Long): Int

}