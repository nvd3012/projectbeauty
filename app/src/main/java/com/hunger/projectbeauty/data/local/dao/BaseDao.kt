package com.hunger.projectbeauty.data.local.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

enum class DAO {
    UPDATE, DELETE, INSERT, SELECT
}

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(obj: T): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(obj: List<T>): List<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(obj: T): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(obj: List<T>)

    @Delete
    suspend fun delete(data: T): Int

    @Delete
    suspend fun delete(data: List<T>): Int

//    suspend fun upsert(obj: T) {
//        val insertResult = insert(obj)
//        if (insertResult == -1L) {
//            update(obj)
//        }
//    }
//
//    suspend fun upsert(objList: List<T>) {
//        val insertListResult = insert(objList)
//        val updateList: MutableList<T> = ArrayList()
//
//        for (i in insertListResult.indices) {
//            if (insertListResult[i] == -1L) {
//                updateList.add(objList[i])
//            }
//        }
//        if (updateList.isNotEmpty()) {
//            update(updateList)
//        }
//
//    }
}