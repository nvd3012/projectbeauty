package com.hunger.projectbeauty.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.hunger.projectbeauty.data.local.dao.AlbumDao
import com.hunger.projectbeauty.data.local.dao.MergeDao
import com.hunger.projectbeauty.data.local.dao.PhotoDao

import com.hunger.projectbeauty.data.local.entity.AlbumEntity
import com.hunger.projectbeauty.data.local.entity.MergeTableEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity

@Database(
    entities = [PhotoEntity::class, AlbumEntity::class, MergeTableEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun photoDao(): PhotoDao
    abstract fun albumDao(): AlbumDao
    abstract fun mergeDao(): MergeDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildData(
                        context
                    )
                        .also { INSTANCE = it }
            }

        private fun buildData(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            "data_local.db"
        ).build()
    }
}