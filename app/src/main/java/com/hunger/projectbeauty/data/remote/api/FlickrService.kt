package com.hunger.projectbeauty.data.remote.api

import com.hunger.projectbeauty.data.model.ResponseDataModel
import okhttp3.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Streaming
import retrofit2.http.Url
import retrofit2.Call


interface FlickrService {


    @GET("rest?extras=date_upload%2Cneeds_interstitial%2Cowner_name%2Cowner_datecreate%2Crealname%2Curl_w%2Curl_m%2Curl_z%2Curl_c%2Curl_l%2Curl_h%2Curl_k&user_id=186814530%40N03&method=flickr.favorites.getList&format=json&nojsoncallback=1")
    suspend fun loadData(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
        @Query("api_key") apiKey: String?
    ): Response<ResponseDataModel>

    @GET
    suspend fun apiKEY(@Url url: String): ResponseBody

    @Streaming
    @GET
    suspend fun downloadImageUrl(@Url url: String) : ResponseBody




    companion object {
        const val BASE_URL = "https://api.flickr.com/services/"
        const val LINK_API_KEY = "https://www.flickr.com/"

    }
}