package com.hunger.projectbeauty.di

import javax.inject.Qualifier


@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class FlickrAPI

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class CoroutineScropeIO

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class CoroutineScropeMain