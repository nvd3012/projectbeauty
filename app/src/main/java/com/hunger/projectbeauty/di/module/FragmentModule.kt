package com.hunger.projectbeauty.di.module

import com.hunger.projectbeauty.ui.album.dialog.AddPhotoDialogFragment
import com.hunger.projectbeauty.ui.album.dialog.AlbumDetailDialogFragment
import com.hunger.projectbeauty.ui.album.fragment.AlbumFragment
import com.hunger.projectbeauty.ui.photo.fragment.PhotoFragment
import com.hunger.projectbeauty.ui.photo_detail.fragment.PhotoDetailDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeRecentFragment(): PhotoFragment


    @ContributesAndroidInjector
    abstract fun contributePhotoDetailDialogFragment(): PhotoDetailDialogFragment

    @ContributesAndroidInjector
    abstract fun contributeAlbumFragment(): AlbumFragment

    @ContributesAndroidInjector
    abstract fun contributeAlbumDetailFragment(): AlbumDetailDialogFragment

    @ContributesAndroidInjector
    abstract fun contributeAddPhotoDialogFragment(): AddPhotoDialogFragment



}