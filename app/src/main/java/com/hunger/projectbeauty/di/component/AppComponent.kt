package com.hunger.projectbeauty.di.component

import android.app.Application
import com.hunger.projectbeauty.BaseApp
import com.hunger.projectbeauty.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, MainActivityModule::class, FragmentModule::class, ViewModelModule::class, ServiceModule::class,AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(baseApp: BaseApp)
}