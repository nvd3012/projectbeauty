package com.hunger.projectbeauty.di.module

import android.app.Application
import com.hunger.projectbeauty.data.local.AppDatabase
import com.hunger.projectbeauty.di.CoroutineScropeIO
import com.hunger.projectbeauty.di.CoroutineScropeMain
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    @Singleton
    @Provides
    fun provideDao(db: AppDatabase) = db.photoDao()


    @CoroutineScropeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    @CoroutineScropeMain
    @Provides
    fun provideCoroutineScopeMain() = MainScope()

}