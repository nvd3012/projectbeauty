package com.hunger.projectbeauty.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hunger.projectbeauty.di.ViewModelFactory
import com.hunger.projectbeauty.di.ViewModelKey
import com.hunger.projectbeauty.ui.album.viewmodel.AlbumViewModel
import com.hunger.projectbeauty.ui.photo.viewmodel.PhotoViewModel
import com.hunger.projectbeauty.ui.photo_detail.viewmodel.PhotoDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory


    @IntoMap
    @Binds
    @ViewModelKey(PhotoViewModel::class)
    abstract fun bindMainViewModel(photoViewModel: PhotoViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(AlbumViewModel::class)
    abstract fun bindAlbumViewModel(albumViewModel: AlbumViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(PhotoDetailViewModel::class)
    abstract fun bindPhotoViewViewModel(photoDetailViewModel: PhotoDetailViewModel): ViewModel



}