package com.hunger.projectbeauty.ui.photo_detail.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.hunger.projectbeauty.databinding.FragmentPhotoViewBinding
import com.hunger.projectbeauty.di.Injectable
import com.hunger.projectbeauty.di.ViewModelFactory
import com.hunger.projectbeauty.di.injectViewModel
import com.hunger.projectbeauty.ui.base.BaseDialogFragment
import com.hunger.projectbeauty.ui.photo_detail.viewmodel.PhotoDetailViewModel
import com.hunger.projectbeauty.utils.AnimationUtils
import com.hunger.projectbeauty.utils.Constants.ID_PHOTO
import javax.inject.Inject

class PhotoDetailDialogFragment : BaseDialogFragment(), Injectable {
    private lateinit var binding: FragmentPhotoViewBinding
    private var idPhoto: String? = null
    private var isShow: Boolean = true

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: PhotoDetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            idPhoto = it.getString(ID_PHOTO)
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentPhotoViewBinding.inflate(
            inflater,
            container,
            false
        ).also { binding = it }.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        viewModel.loadArguments(arguments)
        initEvent()
        showControls()
    }


    fun initEvent() {
        binding.apply {
            lifecycleOwner = this@PhotoDetailDialogFragment
            photoModel = viewModel
            imbBack.setOnClickListener(View.OnClickListener {
                dialog?.cancel()
            })
            imgScale.setOnClickListener(View.OnClickListener {
                if (isShow) {
                    hideControls()
                } else {
                    showControls()
                }
            })
            executePendingBindings()
        }
    }


    fun hideControls() {
        isShow = false
        binding.apply {
            AnimationUtils.animationMoveY(llToolBar, hide = true, moveUp = true)
            AnimationUtils.animationMoveY(llTitleBar, hide = true, moveUp = false)
        }
        dialog?.window?.apply {
            decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    or View.SYSTEM_UI_FLAG_IMMERSIVE)
            addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }

    }

    fun showControls() {
        isShow = true
        dialog?.window?.decorView?.systemUiVisibility = View.VISIBLE
        binding.apply {
            AnimationUtils.animationMoveY(llToolBar, hide = false, moveUp = true)
            AnimationUtils.animationMoveY(llTitleBar, hide = false, moveUp = false)
        }
//        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }
}