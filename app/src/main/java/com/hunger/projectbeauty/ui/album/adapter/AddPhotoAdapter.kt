package com.hunger.projectbeauty.ui.album.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.GlideRequests
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.paging.NetworkState
import com.hunger.projectbeauty.ui.photo.adapter.ListPhotoAdapter
import com.hunger.projectbeauty.ui.photo.adapter.NetworkStateItemViewHolder

class AddPhotoAdapter(
    private val context: Activity,
    private val clickHandler: (PhotoEntity) -> Unit
) :
    PagedListAdapter<PhotoEntity, RecyclerView.ViewHolder>(ListPhotoAdapter.DIFF_CALLBACK) {
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_image_thumb_select_nolike -> AddPhotoViewHolder(
                LayoutInflater.from(parent.context),
                parent
            )
            R.layout.item_network_state -> NetworkStateItemViewHolder.create(parent, {})
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_image_thumb_select_nolike -> {
                (holder as AddPhotoViewHolder).bind(getItem(position), clickHandler, context)
            }
            R.layout.item_network_state -> {
                (holder as NetworkStateItemViewHolder).bindTo(
                    networkState
                )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount-1) {
            R.layout.item_network_state
        } else {
            R.layout.item_image_thumb_select_nolike
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        Log.e("viewType", "itemCount $itemCount")
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                Log.e("viewType", "notifyItemRemoved $itemCount")
                notifyItemRemoved(super.getItemCount())
            } else {
                Log.e("viewType", "notifyItemInserted $itemCount")
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }


    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    fun refreshAdapter(listItemSelected: ArrayList<PhotoEntity>) {
        listItemSelected.forEach {
            it.selected = false
        }
        notifyDataSetChanged()
        listItemSelected.clear()
    }

}