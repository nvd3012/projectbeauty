package com.hunger.projectbeauty.ui.album.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.*
import androidx.paging.PagedList
import com.hunger.projectbeauty.data.local.entity.MergeTableEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.model.AlbumModel
import com.hunger.projectbeauty.data.repository.AlbumRepository
import com.hunger.projectbeauty.data.repository.PhotoRepository
import com.hunger.projectbeauty.di.CoroutineScropeIO
import com.hunger.projectbeauty.ui.base.BaseViewModel
import com.hunger.projectbeauty.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class AlbumViewModel @Inject constructor(
    private val mAlbumRepository: AlbumRepository,
    private val mPhotoRepository: PhotoRepository,
    @CoroutineScropeIO private val scopeIO: CoroutineScope
) : BaseViewModel() {

    private val TAG = "AlbumViewModel"
    private var idAlbum = 0L
    private var liked = MutableLiveData<Int>()
    lateinit var albumModel: AlbumModel

    override fun <T> createArguments(vararg data: T): Bundle {
        val bundle = Bundle()
        if (data[0] is AlbumModel) {
            bundle.putSerializable(Constants.ALBUM_ARGUMENT, data[0] as AlbumModel)
        }
        if (data[0] is PhotoEntity) {
            bundle.putSerializable(Constants.PHOTO_ARGUMENT, data[0] as PhotoEntity)
        }
        if (data[0] is Long) {
            bundle.putLong(Constants.ID_ALBUM, data[0] as Long)
        }
        if (data.size > 1) {
            if (data[1] is Int) {
                bundle.putInt(Constants.LIKED, data[1] as Int)
            }
        }

        return bundle
    }

    override fun loadArguments(arguments: Bundle?) {
        Log.e(TAG, "loadArgument  ")
        arguments?.let {
            try {
                it.getInt(Constants.LIKED).let { like ->
                    Log.e(TAG, "loadArgument $like ")
                    liked.postValue(like)
                }
                idAlbum = it.getLong(Constants.ID_ALBUM)
                albumModel = it.getSerializable(Constants.ALBUM_ARGUMENT) as AlbumModel
                Log.e(TAG, "loadArgument ${albumModel.name} ")
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
    }

    fun createAlbum(
        row: AlbumModel,
        finished: (Long) -> Unit
    ) {
        mAlbumRepository.insertAlbum(scopeIO, row, finished)
    }

    fun deleteAlbumById(
        album: AlbumModel,
        finished: (Int) -> Unit
    ) {
        mAlbumRepository.deleteAlbumByID(scopeIO, album, finished)
    }

    fun updateAlbumName(
        model: AlbumModel,
        finished: (Int) -> Unit
    ) {
        mAlbumRepository.updateAlbumName(scopeIO, model, finished)
    }

    fun getListAlbum(): LiveData<PagedList<AlbumModel>> {
        return mAlbumRepository.getListAlbum()
    }

    fun getPhotosOfAlbum(): LiveData<PagedList<PhotoEntity>> {
        return mAlbumRepository.getPhotosOfAlbum(albumModel.id)
    }

    fun insertMergeTable(
        rows: ArrayList<PhotoEntity>,
        onFinish: (List<Long>) -> Unit
    ) {
        val listMergeTableEntity = arrayListOf<MergeTableEntity>()
        rows.forEach { photo ->
            listMergeTableEntity.add(MergeTableEntity(albumModel.id, photo.id))
            Log.e(TAG, "add data ${photo.id} album $idAlbum")
        }
        Log.e(TAG, "insert ${listMergeTableEntity.size}")
        mAlbumRepository.insertMergeTable(
            scope = scopeIO,
            merges = listMergeTableEntity,
            onFinished = onFinish
        )
    }

    fun deletePhotoInAlbum(
        rows: ArrayList<PhotoEntity>,
        onFinish: () -> Unit
    ) {
        val listMergeTableEntity = arrayListOf<MergeTableEntity>()
        rows.forEach { photo ->
            listMergeTableEntity.add(MergeTableEntity(albumModel.id, photo.id))
            Log.e(TAG, "add data ${photo.id} album $albumModel.id")
        }
        Log.e(TAG, "insert ${listMergeTableEntity.size}")
        mAlbumRepository.deletePhotoInAlbum(scopeIO, listMergeTableEntity, onFinish)
    }


    private val result = Transformations.map(liked) {
        Log.e(TAG, "add data loaddata")
        mPhotoRepository.getPagedListPhotoToAddAlbum(
            albumModel.id,
            it,
            scopeIO
        )
    }
    val networkState = result.switchMap { it.networkState }

    val refreshState = result.switchMap { it.refreshState }

    val pagedList = result.switchMap { it.pagedList }

    fun setIdAlbum(id: Long, mLiked: Boolean) {
//        mutableIdAlbum.postValue(id)
//        liked = mLiked
    }


    override fun onCleared() {
        super.onCleared()
        scopeIO.cancel()
    }

}