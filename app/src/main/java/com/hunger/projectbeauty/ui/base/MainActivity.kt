package com.hunger.projectbeauty.ui.base

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.di.Injectable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), Injectable {
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
        setContentView(R.layout.activity_main)
        askForPermission(WRITE_EXTERNAL_STORAGE, 102)
        initView()
    }


    private fun initView() {
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.navHostFragment) as NavHostFragment? ?: return
        val navController = host.navController
        appBarConfiguration = AppBarConfiguration(navController.graph)

        setupBottomNavMenu(navController)
        navController.addOnDestinationChangedListener(NavController.OnDestinationChangedListener { _, destination, _ ->
//            when (destination.id) {
//                R.id.albumFragment -> {
//                    supportActionBar?.show()
//                }
//                R.id.recentFragment,R.id.favoriteFragment ->supportActionBar?.hide()
//            }
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }

            Log.d("NavigationActivity", "Navigated to $dest")
        })


    }

    private fun setupBottomNavMenu(navController: NavController) {
        bottomNavigation.setupWithNavController(navController)
        bottomNavigation.setOnNavigationItemReselectedListener { }
    }

    private fun askForPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(permission),
                requestCode
            )
        } else if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {
            Toast.makeText(applicationContext, "Permission was denied", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults!!)
        if (ActivityCompat.checkSelfPermission(
                this,
                permissions[0]!!
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            if (requestCode == 101) Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT)
                .show()
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }


}
