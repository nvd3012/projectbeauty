package com.hunger.projectbeauty.ui.base

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView

@Suppress("UNCHECKED_CAST")
abstract class BaseFragment<T : Activity> : Fragment() {
    lateinit var parentActivity: T
    override fun onAttach(context: Context) {
        super.onAttach(context)
        parentActivity = activity as T
    }



}