package com.hunger.projectbeauty.ui.album.dialog

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.dao.DAO
import com.hunger.projectbeauty.data.model.AlbumModel
import kotlinx.android.synthetic.main.dialog_album_popup.*

class DialogAlbumPopup(
    context: Context,
    title: String,
    private val handleEvent: (album: AlbumModel) -> Unit,
    private val albumModel: AlbumModel,
    private val status: DAO
) {
    var dialog: Dialog? = null

    init {
        dialog = Dialog(context)
        dialog?.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.dialog_album_popup)
            tvTitle.text = title
            setCancelable(false)
            window?.apply {
                setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT
                )
            }
            initEvents(this)
            show()
        }
    }

    private fun initEvents(dialog: Dialog) {
        dialog.apply {
            btnCancel.setOnClickListener {
                cancel()
            }
            btnOk.setOnClickListener {
                albumModel.name = edtAlbumName.text.toString()
                albumModel.status = status
                handleEvent(albumModel)
                cancel()
            }
        }
    }

}