package com.hunger.projectbeauty.ui.photo.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.databinding.ItemImageThumbBinding
import com.hunger.projectbeauty.ui.base.DataBindingViewHolder
import com.hunger.projectbeauty.utils.ActivityUtils


class ListPhotoViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : DataBindingViewHolder<ItemImageThumbBinding>(inflater, parent, R.layout.item_image_thumb) {


    fun bind(
        item: PhotoEntity?,
        componentViewOnClick: (String) -> Unit,
        itemOnClick: ((PhotoEntity) -> Unit)?
    ) {
        if (item != null) {
            itemView.setOnClickListener {
                itemOnClick?.let {
                    it(item)
                }
                Log.i("tag", "click")
//                if (activity is MainActivity) {
//                    val dialogFragment = PhotoDetailDialogFragment.newInstance(item.id)
//                    dialogFragment.show(activity.supportFragmentManager, "PhotoDetail")
//                }
            }
            binding.apply {
                photoItem = item
                Log.e("reload view", "${photoItem?.liked}")
                executePendingBindings()
                imgFav.setOnClickListener {
                    componentViewOnClick(item.id)
                }
                itemView.apply {
                    item.setContentItemShow(
                        if (ActivityUtils.isOrentionScreenPortrait(context)) {
                            ActivityUtils.getWidthHeightScreen(context).widthPixels / 2
                        } else {
                            ActivityUtils.getWidthHeightScreen(context).widthPixels / 4
                        }
                    )
//                    item.setContentItemShow(
//                        if (ActivityUtils.isOrentionScreenPortrait(context)) {
//                            ActivityUtils.getWidthHeightScreen(context).widthPixels
//                        } else {
//                            ActivityUtils.getWidthHeightScreen(context).heightPixels
//                        }
//                    )
                    val lp = rlLayout.layoutParams
                    lp.width = item.width
                    lp.height = item.height
//                    if (lp is FlexboxLayoutManager.LayoutParams) {
//                        lp.flexGrow = 1f
//                    }
                }

                llTitleBar.visibility = View.VISIBLE

            }

        }
    }


}
