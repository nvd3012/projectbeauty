package com.hunger.projectbeauty.ui.album.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.databinding.ItemImageThumbSelectNolikeBinding
import com.hunger.projectbeauty.ui.base.DataBindingViewHolder
import com.hunger.projectbeauty.ui.base.MainActivity
import com.hunger.projectbeauty.ui.photo_detail.fragment.PhotoDetailDialogFragment
import com.hunger.projectbeauty.utils.ActivityUtils

class PhotoAlbumViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) :
    DataBindingViewHolder<ItemImageThumbSelectNolikeBinding>(
        inflater, parent,
        R.layout.item_image_thumb_select_nolike
    ) {

    fun bind(
        item: PhotoEntity?,
        clickHandler: (PhotoEntity,Boolean) -> Unit,
        mContext: Activity,
        showSelect: Boolean,
        notifyDataSetChanged: (Boolean) -> Unit
    ) {
        binding.apply {
            llTitleBar.visibility = View.VISIBLE

            item?.let { data ->
                fun onSelected() {
                    data.selected = !data.selected
                    photoItem = data
                    clickHandler(data,true)
                }
                data.setContentItemShow(
                    if (ActivityUtils.isOrentionScreenPortrait(mContext)) {
                        ActivityUtils.getWidthHeightScreen(mContext).widthPixels / 2
                    } else {
                        ActivityUtils.getWidthHeightScreen(mContext).widthPixels / 4
                    }
                )
                val lp = rlLayout.layoutParams
                lp.width = data.width
                lp.height = data.height

                rlLayout.setOnClickListener {
                    if (showSelect) {
                        onSelected()
                    } else {
                        clickHandler(data,false)
                    }
                }
                if (showSelect) {
                    vSelect.visibility = View.VISIBLE
                } else {
                    vSelect.visibility = View.GONE
                    itemView.setOnLongClickListener {
                        notifyDataSetChanged(true)
                        onSelected()
                        return@setOnLongClickListener true
                    }
                }

                photoItem = data
                executePendingBindings()
            }
        }
    }


}