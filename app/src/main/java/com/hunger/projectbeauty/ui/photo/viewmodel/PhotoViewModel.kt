package com.hunger.projectbeauty.ui.photo.viewmodel

import android.content.IntentSender
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.*
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.repository.PhotoRepository

import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.di.CoroutineScropeIO
import com.hunger.projectbeauty.ui.base.BaseViewModel
import com.hunger.projectbeauty.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class PhotoViewModel @Inject constructor(
    private val mRepository: PhotoRepository,
    @CoroutineScropeIO private val coroutineScopeIO: CoroutineScope
) : BaseViewModel() {
    private val TAG = "MainViewModel"
    private var liked = false
    private val queryLiveData = MutableLiveData<String>()

    private val resultData = Transformations.map(queryLiveData) {
        Log.e(TAG, "requestData $it")
        mRepository.getPagedListPhotoLocal(it, liked, coroutineScopeIO)
    }

    val networkState = resultData.switchMap { it.networkState }

    val refreshState = resultData.switchMap { it.refreshState }

    val pagedList = resultData.switchMap {
        Log.e(TAG, "pagedList loadmore")
        it.pagedList
    }


    fun updatePhotoLikeById(idPhoto: String,onFinished: (Int)->Unit) {
        mRepository.updateLikeById(idPhoto, coroutineScopeIO,onFinished)
    }

    fun searchRepo(queryString: String) {
        queryLiveData.postValue(queryString)
    }

    override fun <T> createArguments(vararg data: T): Bundle {
        val bundle = Bundle()
        if(data[0] is PhotoEntity){
            bundle.putSerializable(Constants.PHOTO_ARGUMENT,data)
        }
        return bundle
    }

    override fun loadArguments(arguments: Bundle?) {
        arguments?.let {
            liked = it.getBoolean(Constants.LIKED, false)
        }
    }


    override fun onCleared() {
        super.onCleared()
        Log.e("Image loading", "clear viewmodel")
        coroutineScopeIO.cancel()
    }
}

