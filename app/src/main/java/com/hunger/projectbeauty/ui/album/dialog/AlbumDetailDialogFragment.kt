package com.hunger.projectbeauty.ui.album.dialog

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.MergeTableEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.di.Injectable
import com.hunger.projectbeauty.di.ViewModelFactory
import com.hunger.projectbeauty.di.injectViewModel
import com.hunger.projectbeauty.ui.album.adapter.PhotoAlbumAdapter
import com.hunger.projectbeauty.ui.album.viewmodel.AlbumViewModel
import com.hunger.projectbeauty.ui.base.BaseDialogFragment
import com.hunger.projectbeauty.ui.base.MainActivity
import com.hunger.projectbeauty.utils.ActivityUtils
import kotlinx.android.synthetic.main.dialog_album.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject


class AlbumDetailDialogFragment : BaseDialogFragment(), Injectable {
    private var clearIcon: Drawable? = null
    private var backIcon: Drawable? = null
    private val listItemSelected = arrayListOf<PhotoEntity>()
    private lateinit var mAdapter: PhotoAlbumAdapter
    private val TAG = "AlbumDetail"


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: AlbumViewModel






    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_keyboard_backspace)
        clearIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_clear)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        viewModel.loadArguments(arguments)
        initFirstToolBar()
        initAdapter()
    }


    private fun initAdapter() {
        mAdapter = PhotoAlbumAdapter(requireActivity(), ::handleEvent, ::handleAdd)
        val mLayoutManager = if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
            GridLayoutManager(requireContext(), 2)
        } else {
            GridLayoutManager(requireContext(), 4)
        }
        mLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb_select_nolike -> 1
                        R.layout.item_add_photo -> 2
                        else -> -1
                    }
                } else {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb_select_nolike -> 1
                        R.layout.item_add_photo -> 4
                        else -> -1
                    }
                }

            }
        }
        rvImage.apply {
            adapter = mAdapter
            layoutManager = mLayoutManager
        }
        viewModel.getPhotosOfAlbum().observe(viewLifecycleOwner, Observer {
            rvImage.scrollToPosition(0)
            mAdapter.submitList(it)
            if (it.size != 0) {
                mAdapter.removeAddButton()
            }
        })
    }

    private fun handleEvent(item: PhotoEntity,selected:Boolean) {
    if(selected){
        if (item.selected) {
            listItemSelected.add(item)
            Log.e(TAG, "add ${listItemSelected.size}")
        } else {
            listItemSelected.remove(item)
            Log.e(TAG, "removed ${listItemSelected.size}")
        }
        toolbar.title = "${listItemSelected.size}"
        if (listItemSelected.size != 0 && toolbar.navigationIcon == backIcon
        ) {
            Log.e(TAG, "selected 2 ${listItemSelected.size}")
            initSelectedToolBar()
        } else if (listItemSelected.size == 0) {
            initFirstToolBar()
            mAdapter.refreshAdapter(listItemSelected)
        }
    }else{
        findNavController().navigate(R.id.action_albumDetailDialogFragment_to_photoDetailDialogFragment,viewModel.createArguments(item))
    }



    }

    private fun handleAdd() {
        showDialogAddPhoto()
    }

    private fun initFirstToolBar() {
        toolbar.apply {
            useOnBackSupper = true
            changeMeneToolbar(R.menu.menu_toolbar_album,backIcon)
            title = viewModel.albumModel.name
            setNavigationOnClickListener {
                dismiss()
            }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.itemAdd -> {
                        showDialogAddPhoto()
                        return@setOnMenuItemClickListener true
                    }
                    R.id.select -> {
                        initSelectedToolBar()
                        mAdapter.setShowSelect(true)
                        return@setOnMenuItemClickListener true
                    }

                }
                return@setOnMenuItemClickListener false
            }
        }
    }

    private fun initSelectedToolBar() {
        useOnBackSupper = false
        toolbar.apply {
            changeMeneToolbar(R.menu.menu_button_toolbar,clearIcon)
            setNavigationOnClickListener {
                mAdapter.refreshAdapter(listItemSelected)
                initFirstToolBar()
            }
            menu?.getItem(0)?.title = "DELETE"
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.btnToolbar -> {
                        (activity as MainActivity).apply {
                            if (listItemSelected.size > 0) {
                                showLoading(true)
                                viewModel.deletePhotoInAlbum(listItemSelected) {
                                    showLoading(false)
                                    initFirstToolBar()
                                    listItemSelected.clear()
                                }
                            }
                        }
                        return@setOnMenuItemClickListener true
                    }
                    else -> {
                        return@setOnMenuItemClickListener true
                    }
                }
            }


        }
    }

    private fun changeMeneToolbar(mMenu: Int, icon:Drawable?){
        toolbar.apply {
            navigationIcon = icon
            menu.clear()
            inflateMenu(mMenu)
        }
    }


    private fun showDialogAddPhoto() {
        val items = arrayOf("Recent", "Favorites")
        MaterialAlertDialogBuilder(context)
            .setTitle("Select folder image")
            .setItems(items) { _, which ->
                Log.e("select ", "$which")
                findNavController().navigate(R.id.action_albumDetailDialogFragment_to_addPhotoDialogFragment,viewModel.createArguments(viewModel.albumModel,which))
            }
            .show()

    }

    override fun onBack() {
        mAdapter.refreshAdapter(listItemSelected)
        initFirstToolBar()
    }
}