package com.hunger.projectbeauty.ui.photo.adapter


import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.paging.NetworkState


class ListPhotoAdapter(
    private val componentViewClickListener: (String) -> Unit,
    var itemOnClickListener: ((PhotoEntity) -> Unit)? = null
) : PagedListAdapter<PhotoEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_image_thumb -> {
                val item = getItem(position)
                (holder as ListPhotoViewHolder).bind(
                    item,
                    componentViewClickListener,
                    itemOnClickListener
                )
            }
            R.layout.item_network_state -> {
                (holder as NetworkStateItemViewHolder).bindTo(
                    networkState
                )
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.e("viewType", "type $viewType")
        return when (viewType) {
            R.layout.item_image_thumb -> ListPhotoViewHolder(
                LayoutInflater.from(parent.context),
                parent
            )
            R.layout.item_network_state -> NetworkStateItemViewHolder.create(parent, {})
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.item_network_state
        } else {
            R.layout.item_image_thumb
        }
    }


    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        Log.e("viewType", "itemCount $itemCount")
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                Log.e("viewType", "notifyItemRemoved $itemCount")
                notifyItemRemoved(super.getItemCount())
            } else {
                Log.e("viewType", "notifyItemInserted $itemCount")
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }


    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PhotoEntity>() {
            override fun areItemsTheSame(oldItem: PhotoEntity, newItem: PhotoEntity) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: PhotoEntity, newItem: PhotoEntity): Boolean {
                Log.e("compare", "value old ${oldItem.liked} value new ${newItem.liked}")
                return oldItem.liked == newItem.liked
            }
        }


    }

}