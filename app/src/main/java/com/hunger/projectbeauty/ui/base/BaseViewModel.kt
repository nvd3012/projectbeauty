package com.hunger.projectbeauty.ui.base

import android.os.Bundle
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    open fun <T> createArguments(vararg data: T): Bundle {
        return Bundle()
    }

    open fun loadArguments(arguments: Bundle?) {

    }
}