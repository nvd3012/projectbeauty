package com.hunger.projectbeauty.ui.album.drag_selection

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

abstract class LongPressItemTouchListener(context: Context, private val mListener: () -> Unit) :
    RecyclerView.SimpleOnItemTouchListener() {
    private lateinit var gestureDetector: GestureDetector
    lateinit var mViewHolderInFocus: RecyclerView.ViewHolder
    lateinit var mViewHolderLongPress: RecyclerView.ViewHolder

    init {
        gestureDetector = GestureDetector(context, LongPressGestureListener())
        gestureDetector.setIsLongpressEnabled(true)
    }



    inner class LongPressGestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onLongPress(e: MotionEvent?) {
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            return true
        }

        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
            return true
        }
    }
}