package com.hunger.projectbeauty.ui.photo.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.paging.NetworkState
import com.hunger.projectbeauty.paging.Status
import kotlinx.android.synthetic.main.item_network_state.view.*

class NetworkStateItemViewHolder(
    view: View,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(view) {

    fun bindTo(networkState: NetworkState?) {
        Log.e("viewType", "status $networkState")
        itemView.progressBar.visibility = toVisibility(networkState?.status == Status.RUNNING)
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): NetworkStateItemViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_network_state, parent, false)
            return NetworkStateItemViewHolder(view, retryCallback)
        }

        fun toVisibility(constraint: Boolean): Int {
            Log.e("viewType", "visible $constraint")
            return if (constraint) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
}