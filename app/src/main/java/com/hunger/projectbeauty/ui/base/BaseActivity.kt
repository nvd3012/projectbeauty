package com.hunger.projectbeauty.ui.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.hunger.projectbeauty.data.remote.api.Loading

abstract class BaseActivity : AppCompatActivity() {
    lateinit var loading: Loading

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loading = Loading(this)
    }

    fun showLoading(show: Boolean) {
        loading.show(show)
    }

    fun hideKeyboard(view : View){
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }
}