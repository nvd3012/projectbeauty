package com.hunger.projectbeauty.ui.photo_detail.viewmodel

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.*
import androidx.work.*
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.repository.PhotoRepository
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.data.remote.api.DownloadService
import com.hunger.projectbeauty.data.remote.api.FlickrService
import com.hunger.projectbeauty.di.CoroutineScropeIO
import com.hunger.projectbeauty.ui.base.BaseViewModel
import com.hunger.projectbeauty.utils.Constants
import com.hunger.projectbeauty.utils.Constants.KEY_IMAGE_URL
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import java.io.*
import javax.inject.Inject

class PhotoDetailViewModel @Inject constructor(
    private val mRepository: PhotoRepository,
    application: Application,
    @CoroutineScropeIO private val scopeIO: CoroutineScope,
    private val service: FlickrService
) :
    BaseViewModel() {
    private val workManager = WorkManager.getInstance(application)
    private var notificationManager: NotificationManager? = null
    private var notificationBuilder: NotificationCompat.Builder
    private val context = application.applicationContext
    var photoModel = MutableLiveData<PhotoEntity>()

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            val name = Constants.VERBOSE_NOTIFICATION_CHANNEL_NAME
            val description = Constants.VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(Constants.CHANNEL_ID, name, importance)
            channel.description = description

            // Add the channel
            notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

            notificationManager?.createNotificationChannel(channel)
        } else {
            notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        }

        notificationBuilder = NotificationCompat.Builder(context, Constants.CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_recent)
            .setContentTitle(Constants.NOTIFICATION_TITLE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(LongArray(0))
    }

    override fun loadArguments(arguments: Bundle?) {
        arguments?.let {
            photoModel.postValue(it.getSerializable(Constants.PHOTO_ARGUMENT) as PhotoEntity)
            Log.e("data", "${photoModel.value?.title}")
        }
    }

    fun onClickLike() {
        val model = photoModel.value
        model?.let {
            if (it.liked == 0) {
                it.liked = 1
            } else {
                it.liked = 0
            }
            updatePhoto(it)
        }
        photoModel.postValue(model)
        Log.e("onClick", "${photoModel.value?.liked}")
    }


    private fun createInputDataForImgUrl(): Data {
        val builder = Data.Builder()
        photoModel.value?.let {
            val url = it.url_h ?: it.url_c
            builder.putString(KEY_IMAGE_URL, url!!)
        }
        return builder.build()
    }

    fun updatePhoto(item: PhotoEntity) {
        mRepository.updatePhoto(item, viewModelScope)
    }


    fun downloadAndSaveImageWorkerManger() {
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val task =
            OneTimeWorkRequest.Builder(DownloadService::class.java).setConstraints(constraints)
                .setInputData(createInputDataForImgUrl())
                .build()
        workManager.enqueue(task)
    }


    fun downloadAndSaveImage() {
        scopeIO.launch {
            try {
                makeStatusNotification("Download is processing", false)
                photoModel.value?.let {
                    if (it.urlLocal == null) {
                        val url = it.url_h ?: it.url_c
                        saveToStorage(service.downloadImageUrl(url!!), url)
                    }
                }
            } catch (e: Exception) {
                makeStatusNotification("Download error", false)
                e.printStackTrace()
            }
        }
    }


    private suspend fun saveToStorage(body: ResponseBody?, fileName: String) {
        coroutineScope {
            withContext(Dispatchers.IO) {
                try {
                    val dir = File(
                        context.getExternalFilesDir(
                            null
                        ), "MyPhotos"
                    )
                    if (!dir.exists()) {
                        dir.mkdirs()
                    }
                    val fileImgDownload =
                        File(dir, fileName.substring(fileName.lastIndexOf('/') + 1))
                    var inputStream: InputStream? = null
                    var outputStream: OutputStream? = null
                    try {
                        val fileReader = ByteArray(4096)
                        val fileSize = body?.contentLength()
                        var fileSizeDownloaded: Long = 0

                        inputStream = body?.byteStream()
                        outputStream = FileOutputStream(fileImgDownload)
                        while (true) {
                            val read = inputStream!!.read(fileReader)
                            if (read == -1) {
                                break
                            }
                            outputStream.write(fileReader, 0, read)
                            fileSizeDownloaded += read.toLong()
                            Log.d(
                                "writeResponseBodyToDisk",
                                "file download: $fileSizeDownloaded of $fileSize"
                            )
                        }
                        outputStream.flush()
                        photoModel.value?.let {
                            it.urlLocal = fileImgDownload.absolutePath
                            mRepository.updatePhotoUrLocalById(it, scopeIO) {result->
                                if(result!=0){
                                    photoModel.postValue(photoModel.value)
                                }
                            }
                        }
                        makeStatusNotification("Download finished", true)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        makeStatusNotification("Download error", false)
                    } finally {
                        inputStream?.close()
                        outputStream?.close()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    makeStatusNotification("Download error", false)
                }
            }

        }

    }

    fun makeStatusNotification(message: String, finished: Boolean) {

        // Create the notification
        notificationBuilder.setProgress(0, 0, !finished)
        notificationBuilder.setContentText(message)

        // Show the notification
        notificationManager?.notify(Constants.NOTIFICATION_ID, notificationBuilder.build())
    }


    override fun onCleared() {
        super.onCleared()
//        scopeIO.cancel()
    }
}