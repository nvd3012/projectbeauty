package com.hunger.projectbeauty.ui.album.dialog

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.MergeTableEntity
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.di.Injectable
import com.hunger.projectbeauty.di.ViewModelFactory
import com.hunger.projectbeauty.di.injectViewModel
import com.hunger.projectbeauty.paging.Status
import com.hunger.projectbeauty.ui.album.adapter.AddPhotoAdapter
import com.hunger.projectbeauty.ui.album.viewmodel.AlbumViewModel
import com.hunger.projectbeauty.ui.base.BaseDialogFragment
import com.hunger.projectbeauty.ui.base.MainActivity
import com.hunger.projectbeauty.utils.ActivityUtils
import com.hunger.projectbeauty.utils.showEmptyList
import kotlinx.android.synthetic.main.dialog_album.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject

class AddPhotoDialogFragment : BaseDialogFragment(), Injectable {
    val TAG = "AđPhoto"
    private var clearIcon: Drawable? = null
    private var backIcon: Drawable? = null
    private val listItemSelected = arrayListOf<PhotoEntity>()
    private lateinit var mAdapter: AddPhotoAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    private lateinit var addPhotoViewModel: AlbumViewModel

//    companion object {
//        @JvmStatic
//        fun newInstance(idAlbum: Long, selected: Int) = AddPhotoDialogFragment().apply {
//            arguments = Bundle().apply {
//                putLong(ID_ALBUM, idAlbum)
//                putInt("A", selected)
//            }
//        }
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            idAlbum = it.getLong(ID_ALBUM)
//            folderSelect = it.getInt("A")
//            Log.e("selectAdd ", "$folderSelect")
//            liked = folderSelect != 0
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_keyboard_backspace)
        clearIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_clear)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addPhotoViewModel = injectViewModel(viewModelFactory)
        addPhotoViewModel.loadArguments(arguments)
        initFirstToolBar()
        initAdapter()
    }

    private fun initAdapter() {
        mAdapter = AddPhotoAdapter(requireActivity(), ::handleClick)
        val mLayoutManager = if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
            GridLayoutManager(requireContext(), 2)
        } else {
            GridLayoutManager(requireContext(), 4)
        }
        mLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb_select_nolike -> 1
                        R.layout.item_network_state -> 2
                        else -> -1
                    }
                } else {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb_select_nolike -> 1
                        R.layout.item_network_state -> 4
                        else -> -1
                    }
                }

            }
        }
        rvImage.apply {
            adapter = mAdapter
            layoutManager = mLayoutManager
        }

        addPhotoViewModel.apply {
            pagedList.observe(viewLifecycleOwner, Observer {
                showEmptyList(it.size == 0, tvNoResult, rvImage)
                mAdapter.submitList(it)
                rvImage.apply {
                    val layoutManager = (layoutManager as GridLayoutManager)
                    val positionFirst = layoutManager.findFirstCompletelyVisibleItemPosition()
                    val positionLast = layoutManager.findLastCompletelyVisibleItemPosition()
                    if (positionFirst != RecyclerView.NO_POSITION) {
                        scrollToPosition(positionFirst)
                    } else if (positionLast > 10) {
                        scrollToPosition(positionLast + 1)
                    }
                }
            })

            networkState.observe(viewLifecycleOwner, Observer {
                mAdapter.setNetworkState(it)

                when (it.status) {
                    Status.SUCCESS -> {
                    }
                    Status.FAILED -> {
                        Toast.makeText(requireContext(), it.msg, Toast.LENGTH_LONG).show()
                    }
                    Status.RUNNING -> {
                        showEmptyList(false, tvNoResult, rvImage)
                    }
                }
            })

        }
    }

    private fun handleClick(item: PhotoEntity) {
        if (item.selected) {
            listItemSelected.add(item)
            Log.e(TAG, "add ${listItemSelected.size}")
        } else {
            listItemSelected.remove(item)
            Log.e(TAG, "removed ${listItemSelected.size}")
        }
        toolbar.title = "${listItemSelected.size}"
        if (listItemSelected.size != 0 && toolbar.navigationIcon == backIcon
        ) {
            initSelectedToolBar()
        } else if (listItemSelected.size == 0) {
            initFirstToolBar()
        }
    }

    private fun initFirstToolBar() {
        toolbar.apply {
            title = "Select photo"
            navigationIcon = backIcon
            setNavigationOnClickListener {
                dismiss()
            }
            menu.clear()
        }
    }

    private fun initSelectedToolBar() {
        toolbar.apply {
            navigationIcon = clearIcon
            setNavigationOnClickListener {
                mAdapter.refreshAdapter(listItemSelected)
                initFirstToolBar()
            }
            inflateMenu(R.menu.menu_button_toolbar)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.btnToolbar -> {
                        (activity as MainActivity).apply {
                            if (listItemSelected.size > 0) {
                                showLoading(true)
                                addPhotoViewModel.insertMergeTable(listItemSelected) {
                                    showLoading(false)
                                    dismiss()
                                }
                            }
                        }
                        return@setOnMenuItemClickListener true
                    }
                    else -> {
                        return@setOnMenuItemClickListener true
                    }
                }
            }


        }
    }


}