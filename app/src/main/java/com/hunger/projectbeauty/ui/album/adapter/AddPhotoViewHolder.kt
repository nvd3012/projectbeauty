package com.hunger.projectbeauty.ui.album.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hunger.projectbeauty.GlideRequests
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.databinding.ItemImageThumbSelectNolikeBinding
import com.hunger.projectbeauty.ui.base.DataBindingViewHolder
import com.hunger.projectbeauty.utils.ActivityUtils

class AddPhotoViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) :
    DataBindingViewHolder<ItemImageThumbSelectNolikeBinding>(
        inflater, parent, R.layout.item_image_thumb_select_nolike
    ) {

    fun bind(
        item: PhotoEntity?,
        clickHandler: (PhotoEntity) -> Unit,
        mContext: Activity
    ) {
        binding.apply {
            vSelect.visibility = View.VISIBLE
            item?.let { data ->
                data.setContentItemShow(
                    if (ActivityUtils.isOrentionScreenPortrait(mContext)) {
                        ActivityUtils.getWidthHeightScreen(mContext).widthPixels / 2
                    } else {
                        ActivityUtils.getWidthHeightScreen(mContext).widthPixels / 4
                    }
                )
                val lp = rlLayout.layoutParams
                lp.width = data.width
                lp.height = data.height


                rlLayout.setOnClickListener {
                    data.selected = !data.selected
                    clickHandler(data)
                    photoItem = data
                }
                photoItem = data
                executePendingBindings()

            }
        }

    }

}