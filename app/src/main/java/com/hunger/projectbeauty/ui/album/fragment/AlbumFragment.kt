package com.hunger.projectbeauty.ui.album.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.hunger.projectbeauty.GlideApp
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.dao.DAO
import com.hunger.projectbeauty.data.model.AlbumModel
import com.hunger.projectbeauty.di.Injectable
import com.hunger.projectbeauty.di.injectViewModel
import com.hunger.projectbeauty.ui.album.dialog.DialogAlbumPopup
import com.hunger.projectbeauty.ui.album.adapter.AlbumAdapter
import com.hunger.projectbeauty.ui.album.dialog.AlbumDetailDialogFragment
import com.hunger.projectbeauty.ui.album.viewmodel.AlbumViewModel
import com.hunger.projectbeauty.ui.base.BaseFragment
import com.hunger.projectbeauty.ui.base.MainActivity
import com.hunger.projectbeauty.utils.showEmptyList
import kotlinx.android.synthetic.main.fragment_album.*
import javax.inject.Inject


class AlbumFragment : BaseFragment<MainActivity>(), Injectable {
    private val TAG = "AlbumFragment"
    private lateinit var toolBar: Toolbar

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AlbumViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewModel = injectViewModel(viewModelFactory)
        val view = inflater.inflate(R.layout.fragment_album, container, false)
        toolBar = view.findViewById(R.id.toolbar)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolBar.title = "Albums"

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolBar()
        initAdapter()
        initEvents()
    }

    private fun initToolBar() {
    }

    private fun initEvents() {
        fabCreateAlbum.setOnClickListener {
            DialogAlbumPopup(
                requireContext(),
                "Create album",
                ::handleAlbum,
                AlbumModel(),
                DAO.INSERT
            )
        }
    }

    private fun initAdapter() {
        val mAdapter = AlbumAdapter(handleItemAdapter = ::handleAlbum)
        val mLayoutManager = LinearLayoutManager(requireContext())
        rvAlbum.apply {
            adapter = mAdapter
            layoutManager = mLayoutManager
            setHasFixedSize(true)
        }
        viewModel.getListAlbum().observe(viewLifecycleOwner, Observer {
            Log.e(TAG, "data ${it.size}")
            showEmptyList(it.size == 0, tvNoResult, rvAlbum)
            mAdapter.submitList(it)
        })
    }


    private fun handleAlbum(album: AlbumModel) {
        parentActivity.loading.show(true)
        when (album.status) {
            DAO.UPDATE -> viewModel.updateAlbumName(album) {
                parentActivity.loading.show(false)
            }
            DAO.DELETE -> viewModel.deleteAlbumById(album)
            {
                parentActivity.loading.show(false)
            }

            DAO.INSERT -> viewModel.createAlbum(album) {
                parentActivity.loading.show(false)
                album.id = it
                showDetailDialog(album)
            }
            else -> {
                showDetailDialog(album)
                parentActivity.loading.show(false)
            }
        }
    }


    private fun showDetailDialog(album: AlbumModel) {
        findNavController().navigate(R.id.action_albumFragment_to_albumDetailDialogFragment,viewModel.createArguments(album))
    }


}
