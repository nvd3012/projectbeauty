package com.hunger.projectbeauty.ui.album.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.data.model.AlbumModel


class AlbumAdapter(
    private val handleItemAdapter: (album: AlbumModel) -> Unit
) : PagedListAdapter<AlbumModel, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AlbumViewHolder(LayoutInflater.from(parent.context), parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        (holder as AlbumViewHolder).bind(item, handleItemAdapter)
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<AlbumModel>() {
            override fun areItemsTheSame(oldItem: AlbumModel, newItem: AlbumModel) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: AlbumModel, newItem: AlbumModel): Boolean {
                Log.e("ALO", "${oldItem.name} new ${newItem.name}")
                return oldItem == newItem
            }

        }


    }
}