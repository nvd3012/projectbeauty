package com.hunger.projectbeauty.ui.photo.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.ui.photo.adapter.ListPhotoAdapter

import com.hunger.projectbeauty.utils.Listener
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import com.hunger.projectbeauty.di.Injectable
import com.hunger.projectbeauty.di.injectViewModel
import com.hunger.projectbeauty.paging.Status
import com.hunger.projectbeauty.ui.base.BaseFragment
import com.hunger.projectbeauty.ui.base.MainActivity
import com.hunger.projectbeauty.ui.photo.viewmodel.PhotoViewModel
import com.hunger.projectbeauty.utils.ActivityUtils
import com.hunger.projectbeauty.utils.Constants
import com.hunger.projectbeauty.utils.showEmptyList
import kotlinx.android.synthetic.main.fragment_listphoto.*
import kotlinx.android.synthetic.main.include_card_view_search_bar.*
import java.util.*
import javax.inject.Inject


class PhotoFragment : BaseFragment<MainActivity>(), Injectable {
    private val TAG = "RecentFragment"


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PhotoViewModel
    private lateinit var mAdapter: ListPhotoAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listphoto, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("MainViewModel", "created fragment")
        viewModel = injectViewModel(viewModelFactory)
        viewModel.loadArguments(arguments)
        initAdapter()
        initSearch()

    }

    private fun initSearch() {
        searchView.apply {
            setIconifiedByDefault(false)
            queryHint = "Search for local image title"
        }
        imgVoice.setOnClickListener {
            promptSpeechInput()
        }

        val imgSearch = searchView.getChildAt(0) as LinearLayout
        imgSearch.setOnClickListener {
            viewModel.searchRepo(searchView.query.toString())
        }
        searchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    viewModel.searchRepo(query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()) {
                    viewModel.searchRepo("")
                }
                return false
            }
        })
    }

    private fun initAdapter() {
        mAdapter = ListPhotoAdapter(
            ::componentViewOnClick
        )
        mAdapter.itemOnClickListener = {
            findNavController().navigate(R.id.photoDetailDialogFragment,viewModel.createArguments(it))
        }
//        val flexboxLayoutManager = FlexboxLayoutManager(requireContext()).apply {
//            flexWrap = FlexWrap.WRAP
//            flexDirection = FlexDirection.ROW
//            alignItems = AlignItems.STRETCH
//        }
        val mLayoutManager = if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
            GridLayoutManager(requireContext(), 2)
        } else {
            GridLayoutManager(requireContext(), 4)
        }
        mLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (ActivityUtils.isOrentionScreenPortrait(requireContext())) {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb -> 1
                        R.layout.item_network_state -> 2
                        else -> -1
                    }
                } else {
                    when (mAdapter.getItemViewType(position)) {
                        R.layout.item_image_thumb -> 1
                        R.layout.item_network_state -> 4
                        else -> -1
                    }
                }

            }
        }



        rvPhotos.apply {
            adapter = mAdapter
            layoutManager = mLayoutManager
//            itemAnimator = null
        }
        viewModel.apply {
            searchRepo("")
            pagedList.observe(viewLifecycleOwner, Observer {
                Log.d(TAG, "list: ${it.size}")
                showEmptyList(it.size == 0, tvNoResult, rvPhotos)
                mAdapter.submitList(it)
                rvPhotos.apply {
                    val layoutManager = (layoutManager as GridLayoutManager)
                    val positionFirst = layoutManager.findFirstCompletelyVisibleItemPosition()
                    val positionLast = layoutManager.findLastCompletelyVisibleItemPosition()
                    if (positionFirst != RecyclerView.NO_POSITION) {
                        scrollToPosition(positionFirst)
                    } else if (positionLast > 10) {
                        scrollToPosition(positionLast + 1)
                    }
                }
                parentActivity.hideKeyboard(searchView)
            })
            networkState.observe(viewLifecycleOwner, Observer {
                mAdapter.setNetworkState(it)

                when (it.status) {
                    Status.SUCCESS -> {
                    }
                    Status.FAILED -> {
                        Toast.makeText(requireContext(), it.msg, Toast.LENGTH_LONG).show()
                    }
                    Status.RUNNING -> {
                        showEmptyList(false, tvNoResult, rvPhotos)
                    }
                }
            })
        }
    }


    private fun componentViewOnClick(idPhoto: String) {
        viewModel.updatePhotoLikeById(idPhoto){}
    }


    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH.toString())
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            "Say something..."
        )
        try {
            startActivityForResult(intent, Constants.REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                requireContext(),
                "Sorry! Your device doesn't support speech input",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Constants.REQ_CODE_SPEECH_INPUT == requestCode) {
            if (resultCode == AppCompatActivity.RESULT_OK && null != data) {
                val result = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                result?.let {
                    searchView.setQuery(it[0].replace("", ""), true)
                }
            }
        }
    }


}
