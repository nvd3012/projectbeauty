package com.hunger.projectbeauty.ui.album.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.dao.DAO
import com.hunger.projectbeauty.data.model.AlbumModel
import com.hunger.projectbeauty.databinding.ItemAlbumBinding
import com.hunger.projectbeauty.ui.album.dialog.DialogAlbumPopup
import com.hunger.projectbeauty.ui.base.DataBindingViewHolder

class AlbumViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : DataBindingViewHolder<ItemAlbumBinding>(inflater, parent, R.layout.item_album) {


    fun bind(mItem: AlbumModel?, handler: (album: AlbumModel) -> Unit) {

        binding.apply {
            mItem?.let { model ->
                itemView.setOnClickListener {
                    model.status = DAO.SELECT
                    handler(model)
                }
                tvDelete.setOnClickListener {
                    model.status = DAO.DELETE
                    handler(model)
                }
                tvEdit.setOnClickListener {
                    DialogAlbumPopup(
                        itemView.context,
                        "Replace album name:",
                        handler,
                        AlbumModel(model.id, model.name),
                        DAO.UPDATE
                    )
                }

                item = model
            }
            executePendingBindings()
        }
    }
}