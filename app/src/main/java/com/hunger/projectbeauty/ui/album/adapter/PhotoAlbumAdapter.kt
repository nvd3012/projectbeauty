package com.hunger.projectbeauty.ui.album.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hunger.projectbeauty.R
import com.hunger.projectbeauty.data.local.entity.PhotoEntity
import kotlinx.android.synthetic.main.item_add_photo.view.*
import java.util.ArrayList

class PhotoAlbumAdapter(
    private val context: Activity,
    private val handleEvent: (PhotoEntity,Boolean) -> Unit,
    private val handleAdd: () -> Unit
) :
    PagedListAdapter<PhotoEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    private var showSelect = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_image_thumb_select_nolike -> {
                PhotoAlbumViewHolder(LayoutInflater.from(parent.context), parent)
            }
            R.layout.item_add_photo -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_add_photo, parent, false)
                AddButtomViewHolder(view)
            }
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }


    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1) {
            R.layout.item_add_photo
        } else {
            R.layout.item_image_thumb_select_nolike
        }
    }

    fun removeAddButton() {
        notifyItemRemoved(super.getItemCount())
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_add_photo -> {
                (holder as AddButtomViewHolder).bind(handleAdd)
            }
            R.layout.item_image_thumb_select_nolike -> {
                (holder as PhotoAlbumViewHolder)
                    .bind(
                        getItem(position),
                        handleEvent,
                        context,
                        showSelect,
                        ::setShowSelect
                    )


            }
        }
    }

    fun refreshAdapter(listItemSelected: ArrayList<PhotoEntity>) {
        listItemSelected.forEach {
            it.selected = false
        }
        setShowSelect(false)
        listItemSelected.clear()
    }

    fun setShowSelect(selected: Boolean) {
        showSelect = selected
        notifyDataSetChanged()
    }


    class AddButtomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(handleEvent: () -> Unit) {
            itemView.btnAddImage.setOnClickListener {
                handleEvent()
            }
        }
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PhotoEntity>() {
            override fun areItemsTheSame(oldItem: PhotoEntity, newItem: PhotoEntity) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: PhotoEntity, newItem: PhotoEntity): Boolean {
                return oldItem == newItem
            }

        }


    }

}